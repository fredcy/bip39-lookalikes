This utility generates a list of BIP39 words that look like each other, differing by only one letter.

If reconstructing a crypto wallet from a handwritten mnemonic phrase (list of BIP39 words) does not generate the correct wallet address, perhaps this list could help identify words that are being read incorrectly. 
