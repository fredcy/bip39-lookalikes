// This CLI program reports BIP39 words that are similar to each other.
//
// It reads the BIP39 words from a file (a copy downloaded from
// https://github.com/bitcoin/bips/blob/master/bip-0039/english.txt), considers
// all the pairwise combinations of words, and outputs those pairs that have the
// same length and differ by only one letter

package main

import (
	"fmt"
	"github.com/agnivade/levenshtein"
	"os"
	"strings"
)

func main() {
	content, err := os.ReadFile("bip39.txt")
	if err != nil {
		panic(err)
	}

	lines := strings.Split(string(content), "\n")

	for i, word1 := range lines {
		for _, word2 := range lines[(i + 1):] {
			if len(word1) != len(word2) {
				continue
			}

			distance := levenshtein.ComputeDistance(word1, word2)
			if distance <= 1 {
				fmt.Println(word1, word2)
			}
		}
	}
}
